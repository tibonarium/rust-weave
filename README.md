

# Weave

Проект для прокси сервера сделан `James Wilson <james@jsdw.me>` и доступен по [адресу](https://github.com/jsdw/weave/)

# Сборка и установка

- В проекте используется `async/await`, поэтому нужен `rustc 1.39.0`. Есть есть `toolchain`, то нужная версия в папке проекта устанавливается с помощью
```
rustup override add 1.39.0
```
- Для установки используем следующую команду. Параметр `--force` нужен, чтобы заменить уже установленную версию.
```
cargo install --force --path .
```
- В пакет `MikTex` входит программа с похожим названием `weave.exe`. Поэтому нужно ее убрать из `PATH`.

# Сделанные изменения

- В качестве путей до файла можно использовать абсолютный путь, который начинается с `C`, `D`. Чтобы его можно было использовать на системе, где корневой путь не начинается с /
- Добавлен `clap`, чтобы информация о `routes` можно было хранить в конфигурационном файле, 
а не передавать в качестве аргументов командной строки. Конфигурационный файл называется `Config.toml` и должен находиться в директории, где запускается `weave`.

# Желательные изменения

- Поправить проект, чтобы в качестве конфигурационного файла можно было использовать `weave.toml`. Или чтобы путь до конфигурационного файла можно было передавать через аргумент `--config-file`
- Возможность добавлять в `response` произвольные заголовки. В основном чтобы обходить `CORS`.
- Возможность проксировать соединение, которое создается через `websocket`. С использованием `nodejs` это делается довольно просто.
- Возможность перенаправлять запрос на основе `header` из `request`.

# Образец Config.toml для работы с WWE

```
[[routes]]
source = "=9099/ui/ad/v1/main.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-versions\\wwe\\main.js"

[[routes]]
source = "=9099/ui/ad/v1/config.json"
destination = ".\\config.json"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-altuera-change-config/manifest.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-altuera-override-config-manager\\manifest.js"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-custom-chat/wwe-custom-chat.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-altuera-override-config-manager\\wwe-custom-chat_change_websocket_url.js"

[[routes]]
source = "=9099/ui/ad/v1/lib/backbone/backbone.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-versions\\wwe\\backbone.js"

[[routes]]
source = "=9099/ui/ad/v1/lib/jquery/jquery.cometd.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-versions\\wwe\\jquery.cometd.js"

[[routes]]
source = "=9099/ui/ad/v1/lib/org/cometd.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-versions\\wwe\\cometd.js"

[[routes]]
source = "=9099/ui/ad/v1/lib/jquery/jquery.js"
destination = "C:\\Users\\o.bulatov\\EclipseWorkspace\\OBulatovScala\\wwe-versions\\wwe\\jquery.js"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-eralink-chat/manifest.js"
destination = ".\\dist\\manifest.js"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-eralink-chat/manifest.js.map"
destination = ".\\dist\\manifest.js.map"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-eralink-chat/nls/strings.js"
destination = ".\\dist\\nls\\de-de\\strings.js"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-eralink-chat/nls/custom-strings.js"
destination = ".\\dist\\nls\\custom-strings.js"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-eralink-chat/style/custom-style.css"
destination = ".\\dist\\style\\custom-style.css"

[[routes]]
source = "=9099/ui/ad/v1/module/wwe-eralink-chat/style/fontawesome-all.min.css"
destination = ".\\dist\\style\\fontawesome-all.min.css"

[[routes]]
source = "9099"
destination = "http://192.168.130.34"
```