use serde::Deserialize;
use std::fs;
use toml;
use crate::errors::{ Error };

#[derive(Clone, Deserialize)]
pub struct Config {
  pub routes: Vec<SourceDestination>
}

impl Config {
  pub fn from_file(filename: &str) -> Result<Self, Error> {
      let content: String = match fs::read_to_string(filename) {
        Ok(s) => s,
        Err(e) => return Err(Box::new(e)),
      }; 
      let res = Ok(toml::from_str(&content)?);
      res
  }
}


#[derive(Clone, Deserialize)]
pub struct SourceDestination {
  pub source: String,
  pub destination: String
}
